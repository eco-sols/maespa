<a name="top"></a>

# Technical documentation

* [Execution mode](#exe)

* [Compiling options and optimization](#time)



<a name="exe"></a>
<br/>
<br/>
# [Execution mode](#top)

The current repository of Maespa (supporting a realistic leaf temperature computation) can manage a single run for a single plot, or *simultaneous runs* with multiple plots.

**For a single plot simulation**, name the input files with "0001_" as prefix, and launch the executable in the same directory by providing the number 1 at the begining of the simulation :

<img src="images/running_mode_1.png" width="500" align="center" />

**For a multiple plots simulation**, a such simulation can be justified if the experimental plot is large and let predict a long computing time. In that case, the large plot can be split in further smaller plots (for example 4), corresponding to 4 differents simulations of Maespa, that can be executed at the same time. This concept of 'distributed run' is allowed :
1 - by the balanced distribution of the computing tasks by common multicore computers (such as laptops),  
2 - by the independances of the output data of Maespa, that do not justify a message passing between processes during the execution.

Then, the Maespa source code is not parallelized (no MPI and no OpenMP technologies are implemented). A copy of the sequential executable in the 4 different directories (each corresponding to a subplot) can support a simultaneous execution of 4 processes. 
To subdivide the plot in 4 plots, we suggest to create 4 directories with the specific input files. For example, the first plot can be described by input files located in a directory `dir1` with the prefix `0001_`, the second plot can be described by input files located in `dir2` with prefix `0002_`, and so on.
The prefix number of the input files and output files corresponds to the plot number.
To launch the distributed simulation, open 4 terminals (or 4 occurences of Code Blocks), make sur the executable files are copied in each of the 4 subdirectories, go in these subdurectories and launch the execution in specifying the respective numbers of the subplots:

<img src="images/running_mode_2.png" width="500" align="center" />

In each of the 4 subdirectories will be generated the output files with prefix corresponding to the subplot number.

Another way of launching a distributed simulation implies the setup of a job scheduler on the computer (like [SGE](http://star.mit.edu/cluster/docs/0.93.3/guides/sge.html) or [PBS](https://en.wikipedia.org/wiki/Portable_Batch_System)). Here is an example of a 2 jobs SGE script `launch.sh` :

```bash
#!/bin/bash

# Shell to use
#$ -S /bin/bash
# Name of the job in SGE
#$ -N my_job
# Name of the queue to use
#$ -q cemeb.q
# Maximum hardware time allowed for this job
#$ -l h_rt=00:33:00
# Merge output (error and standard with '-j y') into one single file called my_job.out
#$ -o my_job.out -j y
# run in the current directory
#$ -cwd

-l h_rt=15:00:00
echo "$SGE_TASK_ID" | ./maespa.out
cp maespa.out test$SGE_TASK_ID
cd test$SGE_TASK_ID
./maespa.out
```

It can be launched with the command `qsub -t 1-2 launch.sh`
<br/>
<br/>

<a name="time"></a>
<br/>
<br/>
# [Compiling options and optimization](#top)
A time optimization study has been performed (see the [version report](docs-tech/version-report.md#0.1.4) at the section 0.1.4 for more informations). To allow a high gain of time for long simulations (5 years of experiment with 365 trees), optimizations have been implemented at different levels :
 - source code level : inlining of time costly subroutines and functions have been implemented and the file `maestcom.f90` must be modified by the user according to parameters of the simulation
 - Makefile level : options have been modified to remove debugging features and to take advantage of the processor architectures
 - Choice of the compiler : the use of Intel `ifort` compiler allows faster simulations than with GNU `gfortran`compiler. According to profiling studies, it could be linked to the mathematics librairies (especially for `Pow` and `Exp` functions) that are better adapted to the processor architecture with Intel.


We then suggest at least to modify the `maestcom.f90` file and the compiling options to optimize the source code before launching long simuations.

<br/>

## Compiling options 

|  | **Debug only**<br/>*Increases exec time* | **Highly recommended** | **Release only**<br/>*Decreases exec time* |**For large Maestcom.f90 parameters**|
| ------ | ------ |------ | --------  | --------  | 
| **Intel ifort** | -g<br/>  -check bounds<br/>  -O0<br/> -ftrapuv| -traceback  | -Xhost<br/> -ipo | -mcmodel=medium -shared-intel |
| **GNU gfortran**  | -g<br/> -fcheck=bounds<br/> -O0<br/> -ftrapuv|  | -O3<br/> -flto<br/> -march=native<br/> -finline<br/> -finline-fuctions  | -mcmodel=medium |

 
### Description of several options :

* GNU `-O3` : although it is known to trigger 'agressive optimizations', our simulation tests comparing `O3` option to `O0` option display no differences in the results but faster simulations
* GNU `-finline`: activate inline of components inside they program units
* GNU `-flto` or Intel `-ipo` : activate interprocedural optimizations (such as inlining) across the different source files
* GNU `-march=native` or Intel `Xhost` : improve optimizations at the instruction set level according to the detected processor architecture.

* GNU/Intel `-g` : produces debugging informations in the operating system's native format that can be used by GDB debugger (equivalent of the 'Debug' mode on CodeBlocks or Visual Studio)
* GNU `-fcheck=bounds` or Intel `-check bounds` :option that trigger 'out of bound' errors during runtime. This cautious option has been removed after a verification of the reproductibility of the results because it prevents from using a 'light' `maestcom.f90` file.
* GNU `pg`: this option has the most negative effect on the execution time, since it allows to generate a call graph and flat profile with GProf.
* GNU/Intel `-mcmodel=medium` : if the `maestcom.f90` file must initiate large values of parameters, this option allows to prevent from the compiling error *relocation truncated to fit.*This option is used to compile and link a program whose data and .bss sections exceed 2GB.

More informations can be found at those links for [Intel ifort](https://software.intel.com/content/www/us/en/develop/documentation/cpp-compiler-developer-guide-and-reference/top/compiler-reference/compiler-options/alphabetical-list-of-compiler-options.html) and [GNU gfortran](https://gcc.gnu.org/onlinedocs/gfortran/Option-Index.html#Option-Index).

### Modification of compiling options :  

When using a [**Makefile**](Makefiles/Makefile_Unix), simply add the compiling options in the flag line `FFLAGS` and the compiler type (ifort/gfortran) at the flag line `F90`.

When using an IDE like **CodeBlocks**, first make sure the configuration mode is switched on `Debug` for debugging simulations or `Release` for optimized simulations : 

 <img src="images/Comp_Opt_CB_1.JPG" width="500" align="center" />.
 
Then go in `Settings -> Compiler`,  and tick (or untick) the appropriate options in the `Compiler flags` section. For the options that are not found in this section, write them in the section 
 `Other compiler options` (this is the case for `-finline`, `-finline-functions`, and `-march=native`). Then press `OK`. 
 
  <img src="images/Comp_Opt_CB_2.JPG" width="500" align="center" />.
 
<br/>

## Modifications in maestcom.f90

Currently, arrays in Maespa are statically allocated. Consequently their dimensions must be known before the compiling step to allocate the appropriate amount of memory before the execution.
This system implies the user to estimate the size of arrays before the run and to write them in the following parameters located in the `maestcom.f90` module.

Values of these parameters are often overestimated by users, leading to an increase of the execution time, because the length of arrays are often defining size of loops.
A high gain of execution time stands in the ability to restrict the values of these parameters to the exact need (especially `MAXT` abd `MAXDATE`), according to the input parameters. Here are the corresponding values from the input files :

| **Parameter in maestcom.f90** | **Definition** | **Input file** | **Data bloc** | **Parameter in input file** |
| ------ | ------ |------ | --------  | ------ |
| MAXT | Total number of trees in the plot | *_trees.dat | &plot | notrees |
| MAXLAY | Maximum number of layers for radiation | *to be studied* | *to be studied* | *to be studied* |
| MAXSOILLAY | Maximum number of layers of soil | *to be studied* | *to be studied* | *to be studied* |
| MAXSP | Number of trees species | *_confile.dat | &species | nspecies |
| MAXP | Maximum number of grid points | *_confile.dat and *_trees.dat | &diffang and &plot | notrees x nolay x pplay x 4 |
| MAXC | Maximum number of leaf area distribution | *to be studied* | *to be studied* | *to be studied* |
| MAXANG | Maximum number of zenith and leaf angles | *to be studied* | *to be studied* | *to be studied* |
| MAXD |  *to be studied*  |  *to be studied*  |  *to be studied*  | *to be studied* |
| MAXDATE | Maximum number of dates in the data sequences |  *to be studied* |  *to be studied* |  *to be studied* |

Once these parameters in the `maestcom.f90` module are correctly estimated, save the modifications and recompile the source code.

A current study justifying the branch `maespa_alloc` is ongoing to turn these static allocations in dynamic allocations, with length of arrays read from the Namelists.












