
# Installation of Maespa

Maespa can be compiled with various compiler types. Up to now, only GNU `gfortran` and Intel `ifort` compilers have been used on multiple operating systems.
The Windows based instructions presented are considering a use with GNU `gfortran`, although a use of Intel `ifort` has also been validated with Intel Visual Studio.

Windows users have further options to build maespa with GNU `gfortran`. Among them, we present an installation with MinGW, WSL and CodeBlocks (with CodeBlocks the Makefile is not necessary).
Mac and Linux users can also use CodeBlocks IDE to run Maespa, but a command line process is presented, with the possibility to use either Intel `ifort` compiler, either GNU `gfortran` compiler.

Two Makefiles are then provided in supplementary material : a [Unix Makefile](../Makefiles/Makefile_Unix) for Mac and Linux users (with flags allowing a use of Intel `ifort` or GNU `gfortran` compilers), and a [Windows Makefile](../Makefiles/Makefile_Windows), with a use of GNU gfortran compiler.

  * [Unix command lines installation instructions](docs-tech/install_Linux_Mac.md)

  * [Windows installation instructions with IDE Code Blocks](docs-tech/install_Win_CodeBlocks.md)
  
  * [Windows installation instructions with MinGW](docs-tech/install_Windows_MinGW.md)

  * [Windows installation instructions with WSL](docs-tech/install_Windows_WSL.md)
    
 

# Run of Maespa

Independently of the operating system, the run process of Maespa is similar for all systems using a Unix terminal (Linux, MacOS, MinGW, WSL). We then differ two processes :

   * [Unix command lines execution instructions](docs-tech/run_Linux_Mac.md)

   * [CodeBlocks execution instructions](docs-tech/run-CodeBlocks.md)

