<a name="top"></a>

# Linux and Mac installation instructions

* [Installation of GNU `gfortran` compiler ](#gnu)
* [Installation of Intel `ifort` compiler ](#intel)
* [Compilation ](#compiling)

<a name="gnu"></a>
<br/>
<br/>
<br/>
<br/>

## [Installation of GNU `gfortran` compiler](#top) 

Open a terminal, and type the following commands :

#### Debian / Ubuntu 

```bash
sudo apt-get install gfortran
```
#### CentOS / RHEL 

```bash
yum install epel-release
yum install gcc-gfortran
```

#### OpenSUSE 

```bash
sudo zypper install gcc gcc-c++ gcc-fortran
```

#### on MacOS / OSX

```bash
brew install gcc
```

On all platforms, to test if GNU `gfortran` installation is working, open a terminal and type :
```bash
gfortran
```
The message must then be :
```bash
gfortran: fatal error: no input files
```

<a name="intel"></a>
<br/>
<br/>
<br/>
<br/>
## [Installation of Intel `ifort` compiler](#top) 

Since this Intel compiler has a cost, we suggest to benefit from the [free 1 month trial license](https://software.seek.intel.com/parallel-studio-xe?cid=em-elq-39623&elq_cid=3178284).
The trial license provided is the Intel® Parallel Studio XE 2020 Cluster Edition (the most expensive one) but the Intel® Parallel Studio XE 2020 Composer Edition for Fortran is sufficient for Maespa. For a named license at 599 $, it compiles Fortran and C++ if needed (even if C++ is not used in Maespa).

Here are the main steps to install the Intel license (more precise informations can be found on [Intel website](https://software.intel.com/)):

- make a demand for a 1 month trial and download the `parallel_studio_xe_2020_cluster_edition.tar.gz` file provided by an email from Intel (be careful, this email can be considered as an advertising). This email also provides the serial number:

<img src="images/Intel_1.png" width="500" align="center" />

- open an account on Intel website, register the product with the email and the serial number, in the registration center :

<img src="images/Intel_0.png" width="500" align="center" />

- Obtain the license file : sign in to the Intel account. Then, go to the *Content* section and click on the serial number (in blue) of the registered product. A blue frame `Manage license` appears:

<img src="images/Intel_2.png" width="500" align="center" />

Click on that frame, and a section `license file` dedicated to your product appears with a little mail icon. Click on the mail icon to ask a license file by email. The license file will then be sent by email :

<img src="images/Intel_3.png" width="500" align="center" />

From you emails software, download that file (the name of this license file is something like `COM_L__CPPFOR_LDZT_9XN2HZB8.lic`). Copy that file in a directory of the computing machine (for example in the `parallel_studio_xe_2020_cluster_edition` directory) and keep the path in memory, it will be needed in the next step.

- on the computing machine, create a directory `/tmp/psxe-staging-area` :
```bash
mkdir /tmp/psxe-staging-area
```
- copy the `parallel_studio_xe_2020_cluster_edition.tar.gz` file inside this directory and extract it :
```bash
cp parallel_studio_xe_2020_cluster_edition.tar.gz /tmp/psxe-staging-area
cd /tmp/psxe-staging-area
tar -xvf parallel_studio_xe_2020_cluster_edition.tar.gz
```

- go in the new directory `parallel_studio_xe_2020_cluster_edition` that have just been created and execute the script `install.sh`:
```bash
cd parallel_studio_xe_2020_cluster_edition
./install.sh
```
Then answer the questions :
1. Sign the license terms by writing `accept`
2. Choose `I do not consent the collection of my information`
3. Choose `Skip prerequists`
4. Choose `Activate with license file` and indicate the path where this file has been located.

Once the compiler is installed, the pathes of the files `compilervars.sh` and `ifortvars.sh` must be indicated in the `.profile` file. To do so, for a Linux installation, introduce the following lines in the `.profile` file:
```bash
source /home/blitz/intel/bin/compilervars.sh -arch intel64 -platform linux
source /home/blitz/intel/bin/ifortvars.sh -arch intel64 -platform linux
```

Save the modifications of `.profile` and source it by typing :
```bash
source .profile
```

To test if Intel `ifort` installation is working, open a terminal and type :
```bash
ifort
```
The message must then be :
```bash
ifort: command line error: no files specified; for help type "ifort -help"
```

<a name="compiling"></a>
<br/>
<br/>
<br/>
<br/>

# [Compilation](#top) 

- After the installation of the compiler, download the sources ([src](../src)) and the [Makefile for Unix](../Makefiles/Makefile_Unix), or clone the current repository. To do so, open a terminal and type :

```bash
git clone https://gitlab-ecosols.cirad.fr/blitzfrayret/maespa.git
```

If *git* is not installed yet, the installation instructions can be found [here](https://git-scm.com/downloads). 

- Rename the Makefile for Unix called `Makefile_Unix` in `Makefile` and copy it in the directory where the source files are located (for example, directory *'src'*). 
- Go in the directory where the source files and the Makefile are located, open the Makefile with a text editor and check :

    1. the flag `F90` to activate the use of the compiler.

    For a use of GNU gfortran :
    ```bash
    F90 = gfortran
    ```

    For a use of Intel ifort :
    ```bash
    F90 = ifort
    ```

   2. the flag `FFLAGS` with the compiling options.

    For a use of GNU gfortran :
    ```bash
    FFLAGS = -finit-local-zero -finline-functions -Wuninitialized -ftrapv -ffree-form -ffree-line-length-none -O3 -finline -flto -march=native
    ```

    For a use of Intel ifort :
    ```bash
    FFLAGS = -free -xhost -ipo 
    ```
    And save modifications of the Makefile.
    
Note that compiling options can be modified for debug or optimization. Refer to this [link](docs-tech/docs-tech.md#time) for more informations.     

- Proceed to the compilation :

```bash
cd src
make clean
make
```
The executable file `maespa.out` is then created along with the objects files with extention *.o and *.mod. If the source files are modified, proceed to a clean of the object files with `make clean` before a new compilation with `make`:
```bash
make clean
make
```



