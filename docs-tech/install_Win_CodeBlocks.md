# CodeBlocks setup for Windows

- Go on [CodeBlocks website](http://www.codeblocks.org/downloads) and click on `Download the binary release`. Download the MinGW installer that includes GNU compiler : `codeblocks-20.03mingw-setup.exe`.

- Double click on the downloaded file *codeblocks-20.03mingw-setup.exe* and accept default choices in the installation sequence. Make sure `MinGW Compiler Suite` is ticked :

<img src="images/CB_Win_1.png" width="500" align="center" />

<img src="images/CB_Win_2.png" width="500" align="center" />


- Check the librairies links of the compiler by selecting `Settings -> Compiler -> Toolchain executables` :

<img src="images/CB_Win_3.png" width="500" align="center" />


