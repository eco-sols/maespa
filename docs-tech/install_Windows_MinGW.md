*Authored by R. Vezy, slightly modified by C. Blitz Frayret*

Install MinGW (or Cygwin). There are two main versions of MinGW, but we will use MinGW for Win64 as advised from the [GCC wiki](https://gcc.gnu.org/wiki/GFortranBinaries). Follow these steps to install it:

   1. Download the installer from [here](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download), and install it.    
   2. Add it to your path: press the `Windows key`, type `environment`, press `enter`, click on `environment variables`, click on `path,` and `New`, add the complete path of installation until the bin folder, e.g. if you installed `mingw-w64` in `Program Files`:  `C:\Program Files\mingw-w64\x86_64-8.1.0-posix-seh-rt_v6-rev0\mingw64\bin`. The bin folder may be at a different path on your computer.  
   1. Rename `mingw32-make.exe` from the bin folder into `make.exe`, and also `mingw32-gfortran.exe` into `gfortran.exe`. 
   2. Make sure the Makefile is located at the root of the project. To do so, download the [Makefile for Windows](Makefiles/Makefile_Windows) and rename it as `Makefile`. Copy it above the directory `src`.

Then to build maespa:
   1. open a command or a Powershell at the root of the maespa project, and type:
   ```bash
   shell
   make all
```
    
   `make` follows the instructions from the `Makefile` and builds the project using the MinGW version of `gfortran`. The object files and executable `maespa.exe` are created in a `build` directory. The maespa executable that is produced works directly for your windows operating system.
   
   2. After a source file modification, and before renewing the build with `make all`, we suggest to remove the files in the build directory by typing:
```bash
make clean
```
