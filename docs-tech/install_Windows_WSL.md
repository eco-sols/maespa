*Authored by R. Vezy, slightly modified by C. Blitz Frayret*

Install WSL (Windows Sub-system for Linux) and use the Linux tools to build a Linux-compatible executable:

1. follow [this tutorial](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to install WSL and a Linux distribution (such as Ubuntu);  
1. install `gfortran` and `make` from your linux distribution (e.g. open Ubuntu, or open a terminal and type `bash`):
```bash
sudo apt install gfortran make
```

1. Make sure the Makefile is located at the root of the project (above the `src` directory). To do so, download the [Makefile for Windows](Makefiles/Makefile_Windows) and rename it as `Makefile`.
1. open a terminal at the root of the maespa project, and type:
```bash
make all
```
`make` follows the instructions from the `Makefile` and builds Maespa in the `build` directory using the Linux version of `gfortran`. The maespa executable that is produced works only for Linux, so you can only use it from your Linux sub-system.

2. After a source file modification, and before renewing the build with `make all`, we suggest to remove the files in the build directory by typing:
```bash
make clean
```