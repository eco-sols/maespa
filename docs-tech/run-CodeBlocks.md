# Creation of a CodeBlocks project for Maespa

### Initialization

- Click on `File -> New -> Project` select the icon `Fortran application` and click on `Go`. Then click `Next` and provide the adress, name and title of the project. Click then on `Next` to validate :

- Select the `GNU Fortran compiler` and click on `Finish`. In this example the project `Maespa_formation` is created :

<img src="images/CB_Win_5.png" width="500" align="center" />

- An existing project can be opened by selecting `File -> Open` and searching the name of the project with the file extention `cbp`.

### Configuration

- Download the source files from the [src](../src) directory of this Maespa repository.

- Add the source files to the CodeBlocks project by right-clicking on the project name located in the `Project` work space. Then select `Add files`. Find the *.f90 source files in the local directory where they have been downloaded, an click on `Open` to import them in the project :

 <img src="images/CB_Win_6.png" width="500" align="center" />

- Click `OK`, a sign '+' appears near the project name in the 'Project' workspace. The tree can be unrolled to vizualise imported source files.
 
- For a first use of Maespa, we suggest to set the compiling options to the `Debug` mode by choosing the following options in `Project -> Build options` (even if it slows down the simulation):

- Finally, provide informations about the executable and the object files in the  `Project -> Properties -> Build target` frame and validate by clicking `OK` :

 <img src="images/CB_Win_10.png" width="500" align="center" /> 
 
 Note that the executable file must be executed in the directory where the input files (000*_confile.dat, 000*_watpars.dat, 000*_met.dat, 000*_tutd.dat, 000*_phy1.dat, 000*_str1.dat, 000*_trees.dat) are located.

### Modify, build, run

- To modify a file, open it by double-clicking on its name in the project workspace (the content of the file appears on the screen) and save the modifications by clicking on the floppy disk :

- The build of the source files corresponds to the compiling step (translation of each source file in a machine object langage) and the linking edition where links between procedures (functions, subroutines) are generated. To build the sources, click on `Build -> Build` or on the icon of the yellow gear. 
 In the current example, objects files are created in the directory *'Maespa_formation/obj'* and the executable file in the directory  *'Maespa_formation/bin'*. The build is complete when the mention *'Build finished: 0 error'* appears on the screen.
 Each time a source file is modified, a new executable file must be created by renewing the build of the source files.

- To execute Maespa, click on `Build -> run` on click on the right triangle. A terminal appears, where the number of the studied plot must be written, and click `enter` (the number is 1 if there is only 1 plot in the experiment) :

<img src="images/CB_Win_13.png" width="500" align="center" /> 

The execution of Maespa generates the following output files :  *_Dayflx.dat, *_watlay.dat, *_hrflux.dat, *_watsoilt.dat, *_swplay.dat, *_wattest.dat, *_watbal.dat, *_watupt.dat, *_watbalday.dat, *_Maeserr.dat.
Note that the file `*_Maeserr.dat` is generated only if an error linked to a bad parametrization occurs (system errors are not related in this file).
