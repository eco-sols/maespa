# Version report of Maespa
This report describes the main commits made on the source code since this repository have been created.
The described commits are those affecting the source code or the compilation. Commits related to documentation, pre/post processing, versionning, etc ... are not justifying a version upgrade.

<a name="top"></a>

| **Version** | **Commit author** | **Date** | **Title** |
| ------ | ------ |------ | --------  | 
| [0.2.7](#0.2.7) | Celine Blitz Frayret | 05-01-2022 |"Interpolatep displacement for time optimization" | 
| [0.2.6](#0.2.6) | Celine Blitz Frayret | 30-06-2021 | "Modification header Dayflx/hrflux, revert alloc modifications" |
| [0.2.5](#0.2.5) | Celine Blitz Frayret | 27-04-2021 | "Add of a range of time where *hrflux.dat file is generated" |
| [0.2.4](#0.2.4)| C. Blitz Frayret and C. Diop | 25-04-2021 | "Modification of TOTCO2, TOTRESPF and TOTRESPWM allocations" |
| [0.2.3](#0.2.3)| C. Blitz Frayret and C. Diop | 20-04-2021 | "Modification of ITARGETS and ISPECIES allocations" |
| [0.2.2](#0.2.2)| C. Blitz Frayret and C. Diop | 13-04-2021 | "Add of a CI script and modification of XYCOORDS allocation" |
| [0.2.1](#0.2.1) | Celine Blitz Frayret | 01-02-2021 | "Unactivation of a condition in ZBRENT" |
| [0.2.0](#0.2.0) | Celine Blitz Frayret | 27-01-2021 | "Add dew activation option" + "Correctif watbal.f90" |
| [0.1.6](#0.1.6) | Celine Blitz Frayret | 21-01-2021 | Modifications of plantwater allocation and maestcom parameters |
| [0.1.5](#0.1.5) | Celine Blitz Frayret | 20-01-2021| "Bug fixed for KHRS" | 
| [0.1.4](#0.1.4) | Celine Blitz Frayret | 19-01-2021 | Time optimization | 
| [0.1.3](#0.1.3) | Celine Blitz Frayret | 08-01-2021 | "Modification of a WETTINGBOT allocation" | 
| [0.1.2](#0.1.2) | Celine Blitz Frayret | 05-01-2021 | "Output files modifications and add of a version report" | 
| [0.1.1](#0.1.1) | Rémi Vezy | 11-2019 | 4 different commits | 
| [0.1.0](#0.1.0) | Celine Blitz Frayret |16-01-2019 | "Initial commit" |

**Note : Maespa version number is based on the ‘Major.Minor.Patch’ format. 
The major version number is updated manually when major development is completed such as a deletion of usage features. This is a major change because in that case, the software is not backward compatible anymore. 
The minor version number is updated  when backward compatible features are committed (add of new features). 
The patch number is manually increased each time a bug fix or a minor modification is committed, such as memory or time optimizations.**

 






<br/>
<br/>
<a name="0.1.0"></a>
<br/>
<br/>

# Version [0.1.0](#top) - Commit n° 43c46c0d - "Initial commit"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
16-05-2019

#### **Compiler and configuration used**  
No validation tests performed 

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

*Citation from the file News.md :* 

First release of the code based on the Bitbucket repository branch from the historic repository from R. Duursma and B. Medlyn that already implemented the following changes :  
- voxel-scale leaf evaporation (EV) 
- Computation of canopy air temperature and vapor pressure. 
- Iterations on leaf temperature until convergence 
- Aerodynamic conductance computation at plot scale (used for soil + canopy air temperature and vapor pressure) 
- Allowing two wind extinction profile if ZHT (measurement height) is below canopy height (mainly used in agroforestry with measurements below the shade trees) 
- Bug fixes on slope computation  

The current master branch of MAESPA in this repository (version `0.1.0`) is also
implemented with the following minor changes by Céline Blitz Frayret (modifications in the source code are tagged  'CBF'): 
- Add of run timing screen prints (maespa.f90)
- Modifications of the static allocations of the following variables in files maindeclarations.f90 and watbal.f90 : wettingbot, wettingtop, plantwater 
- Modification of the value of maxdate parameter in maestcom.f90 
- Inactivation of a section of watbal.f90 to prevent from dummy initial values in the output *_watbal.dat file
- Modification of the allocation of plantwater in maindeclarations.f90 :  REAL PLANTWATER(MAXDATE+1, MAXT) ! CBF changed PLANTWATER(MAXDATE, MAXT)
Note that those modifications prior to the initial commit have been done to allow 5 years simulations on the computing platform MBB 


#### **New features**  
 A screen print of the time of the simulation appears at the end of the execution 
 
#### **Usage of new features** 
--

#### **Source code modifications** 
See above description : maespa.f90, maindeclarations.f90, watbal.f90 have been modified

#### **Code testing** 
No tests performed  

#### **Documentation modifications** 
--

#### **Bug fixed** 
See above description 

#### **Notes** 
The version number of this commit has been assigned  monthes after this commit
<br/>
<br/>
<a name="0.1.1"></a>
<br/>
<br/>

# Version [0.1.1](#top) - Commits n°f4fda2fa/dd8fe429/039a6e1b/895b913e - 4 different commits

#### **Commit author**
R. Vezy

#### **Commit date** 
11-2019

#### **Compiler and configuration used**  
No validation tests performed 

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

*Citation from the file News.md :*

- Added a `NEWS.md` file to track changes. 
- Add semantic versioning as of semver specifications(https://semver.org/) of the form `MAJOR.MINOR.PATCH` 
- The Makefile is now compatible with Windows OS. 
- Fix error in parameter reading for the Medlyn stomatal conductance model: `G0` and `G1` were expected for `CO2`, but the model said it was expected for `H2O`, and it did not perform any conversion as for the other models. 
- `EV` can be negative when there is dew but not ET. 
- Re-activate the initialization values in the `watbal.dat` (can be informative for the user, but mostly because `Maeswrap::readwatbal()` handles only the file when this line is present.
- Add support for amphistomatous leaves
- Add a logo (logo/MAESPA.png) 

#### **New features**  
- Amphistomatous leaves
- Possibility of EV negative when dew occurs
 
#### **Usage of new features** 
--

#### **Source code modifications** 
The source files modified are the following :
- inout.f90 to fix the errors related to  the read of Medlyn conductance parameters 
- physiol.f90 for the modifications of EV when dew is activated.
- inout.f90 and physiol.f90 to take into account bouth sides of leaves exchanges for amphisomatous leaves

#### **Code testing** 
A hand made testing has been performed by comparing the new output files to those obtained with a reference test. The reference test correspond to the sources of the initial commit 43c46c0d. The input files are those involved in the paper Vezy et al., 2018, allowing a simulation for the plot 1 with a realistic air temperature calculation in the canopy of an eucalyptus forest, during 5 years. The compilation is performed with Intel compiler so as to perform those simulations in 2 days maximum.

Following simulations have been compared to the results of the initial commit :
- TEST-INITIAL-COMMIT -> commit 43c46c0d
- TEST-G0-G1 : initial commit + fix of conductance parameters read in Medlyn model -> commit dd8fe429 to which the modification of the commit f4fda2fa have been removed
commit dd8fe429
- TEST_FIX_DEW : initial commit + fix of conductance parameters read in Medlyn model+modif ET -> commit 039a6e1b
- TEST_AMPHIS_LEAVES : initial commit + fix of conductance parameters read in Medlyn model+modif ET+amphistomatous leaves → commit 039a6e1

The results are similar for all the output files, except slight rounded errors. The vizualisation of several output variables are similar (see document ‘Comparison-first-commits.pdf’)

#### **Documentation modifications** 
--

#### **Bug fixed** 
Modification of the conductance parameters G0, G1, G20, G21 given for water in inout.f90 

#### **Notes** 
The version number of those commits has been assigned  monthes after commits

<br/>
<br/>
<a name="0.1.2"></a>
<br/>
<br/>

# Version [0.1.2](#top) - Commit n°8e2217f9 - "Output files modifications and add of a version report"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
05-01-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Add of this ‘version-report.docx’ file to describe each commit, in supplement to the NEW.md file.
Minor modifications of output files :
1. Re-re-activation of the suppression of initial dummy values ‘-999’ in *watbal.dat file. If this prevent from a good usage of the vizualisation package Maeswrap, a fixing of Maeswrap can be considered
2. Bug fixing in output file *Dayflx.dat because format error representated by stars characters ‘****’ appear in the last column of canopy evaporation for long simulations
3. The 13th variable outputed in file *Dayflx.dat (TOTH2OEV(ITAR)) is not explained in the header, this has been added.


#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 
1. In watbal.f90, the initial writing of ZEROVAR values in ‘UWATBAL’ file has been commented
3. In inout.f90, add of the ‘totH2OEV’ label in the header, with the corresponding explanation. The format 5122 have been added.

#### **Code testing** 
A hand made testing has been performed by comparing the new output files to those obtained with a reference test. The reference test correspond to the sources of the previous commit 0e16a2f2. The input files are those involved in the paper Vezy et al., 2018, allowing a simulation for the plot 1 with a realistic air temperature calculation in the canopy of an eucalyptus forest, during 7 days. The compilation is performed with GNU compiler.
The results are similar for all the output files except for : 
- the *Dayflx.dat files where the accuracy of 10 variables has been improved and the totH2OEV variable has been documented in the header,
- the *watbal.dat files where the first  line dummy values ‘-999’ have been removed
The resolution of the problem of format of the totH2OEV variable in *Dayflx.dat file has been validated with the run of a 5 years simulation test.

#### **Documentation modifications** 
--

#### **Bug fixed** 
2. In inout.f90, the format 500 of the writing of UDAILY has been modify to allow a correct print of the variable ‘TOTH2OEV(ITAR)’.  The  format of the 9 previous printed variables has also been modified: the format F16.6 has been assigned to those variables print. Then the printed values are more accurate in the *Dayflux.dat file.

#### **Notes** 
**As suggested by Remi Vezy, Maespa keeps on using the ‘Major.Minor.Patch’ format as version number. 
The major version number is updated manually when major development is completed  such as a deletion of usage features. This is a major change because the software is not backward compatible anymore. The minor version number is updated  when backward compatible features are committed (add of new features). The patch number is manually increased each time a bug fix or a minor modification is committed, such as memory or time optimizations.**

<br/>
<br/>
<a name="0.1.3"></a>
<br/>
<br/>

# Version [0.1.3](#top) - Commit n°c43f2469  - "Modification of a WETTINGBOT allocation"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
08-01-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Modification of a ‘wettingbot’ allocation that was missing to prevent from a runtime error.

#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 

In watbal.f90, the declaration ‘real wettingbot(10)’ has been replaced by ‘real wettingbot(100)’ 

#### **Code testing** 
A hand made testing has been performed by comparing the new output files to those obtained with a reference test. This reference test corresponds to the source code of the previous commit 8e2217f9. The input files are those involved in the paper Vezy et al., 2018, allowing a simulation for the plot 1 with a realistic air temperature calculation in the canopy of an eucalyptus forest, during 7 days. The compilation is performed using GNU compiler.
The resulting otuput files are similar for both simulations.

#### **Documentation modifications** 
--

#### **Bug fixed** 
This modification prevent from the runtime error ‘array wettingbot out of bound’ that occured during a run in the context of the PhD of Sidy Sow.

#### **Notes** 
--
<br/>
<br/>
<a name="0.1.4"></a>
<br/>
<br/>

# Version [0.1.4](#top) - Commit n°5ab68331 / 39cc7e8b  - Time optimization

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
19-01-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Modifications in the source code and the Makefile allowing to save execution time, with Intel and GNU compilers.

First, a high gain of time is made when the maestcom.f90 parameters are smallest as possible, especially for MAXT parameters (total number of trees in the simulation, refered in the confile.dat input file) and MAXDATE parameter (maximum number of dates of input data sequences). This modification of maestcom.f90 is possible when the options ‘check bounds’ of the Makefile (with Intel or GNU) is removed. We then provide an appropriated  Makefile by this commit. A such ‘check bounds’ option triggers runtimes error when maestcom.f90 is ‘light’. However, we did not modify the maestcom.f90 file of this commit because its parameters values are depending on the simulation test case. The user will have to change these values and recompile the source code for best performances.

Another way of earning a high amount of time is to choose Intel compiler. Indeed, profiling studies with Maqao and Gprof profilers showed that the most time consuming components are ‘exp’ and ‘pow’ functions (see Maespa_optimisation.docx). Those functions are provided by the compilers, so a well optimized compiler like Intel is recommended.

Finally, source code modifications also lead to a gain of time when inlining several components. Indeed, profiling studies of long and short simulations (see Maespa_optimisation.docx) pointed that 4 components were frequently called during runs : PHOTOSYN subroutine, TK, SATUR and QUADP functions. Since the system time consuming in those calls is high, the tested strategy of integrating those components into their parent bodies (inlining) confirmed the gain of execution time.  This commit implements such modifications.

Example of execution time gains for a simulation of 5 years, plot 1 Vezy et al., 2018 :

| **Code modifications** | **GNU compiler** | **Intel compiler** |
| ------ | ------ |-----| 
| Code and makefile from previous Bitbucket repository | 525425 s | > 1 week (86 days simulated in 3.5 days of execution) | 
|Makefile options + maestcom MAXT/MAXDATE | 292168 s| 149185 s|
|Makefile options + maestcom MAXT/MAXDATE + inlining technique in source code | 259694 s | 125000 s |

#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 

- Add of a Makefile (in src directory) with appropriate Gnu and Intel  options to optimize execution time
- Modifications of the following source files to allow SATUR, TK, QUADP and PHOTOSYN integration to their parent components (inlining strategy) : getmet.f90, watbal.f90, maespa.f90, physiol.f90, utils.f90 and radn.f90

For more information, please refer to ‘Maespa_optimisation.docx’

#### **Code testing** 

Hand made testing has been performed by comparing new output files to those obtained with 7 days and 5 years reference tests compiled with Gnu and Intel compilers. Those reference tests correspond to the source code of the initial commit. The input files are those involved in the paper Vezy et al., 2018, allowing a simulation for the plot 1 with a realistic air temperature calculation in the canopy of an eucalyptus forest. 
In each test case (modifications in the compilers, or in their options, or in the maestcom or in the source code) resulting otuput files are similar for all simulations to the initial commit, except for slight differences in rounding of numbers. For more information, please refer to ‘Maespa_optimisation.docx’

#### **Documentation modifications** 
--

#### **Bug fixed** 
--

#### **Notes** 
--
<br/>
<br/>
<a name="0.1.5"></a>
<br/>
<br/>

# Version [0.1.5](#top) - Commit n° 6a55a565 - "Bug fixed for KHRS"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
20-01-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Bug fix of KHRS variable. This variable is the time step read from the *met.dat input file. It is read as a ‘METFORMAT’ namelist in getmet.f90 and used in the READLAT subroutine. 
The problem was that READLAT (that uses KHRS) was called before the *met .dat file parameters was read. Then, the value of KHRS used was 0 instead of the actual value written in *met.dat. 

#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 

In getmet.f90 we moved the ‘call READLAT’ instruction after the read of the namelist METFORMAT, so that the actual value of KHRS can be used in the program.

#### **Code testing** 

Hand made testing has been performed by comparing new output files to those obtained with 7 days and 5 years reference tests compiled with Gnu and Intel compilers. Those reference tests correspond to the source code of the previous commit 39cc7e8b. The input files are those involved in the paper Vezy et al., 2018, allowing a simulation for the plot 1 with a realistic air temperature calculation in the canopy of an eucalyptus forest. 

We can note significant changes in the results :

- the execution time decreased from ~135000s to ~53000s, probably because of the time step that was previously considered at 0 and that is now the real time step (48 subdivisions of an hour in this test case)

- the following output variables (from *watbal.dat file) have significantly changed :
* all the radiations values (qn, qe, qc, qh) are twice larger than the previous simulation
* simulated soil evaporation have increased
* simulated canopy transpiration have decreased
* discharge and wsoil have increased and their curve shapes are modified

Maybe others modifications could be noticed since we plotted only results from the *watbal.dat file.

***Nevertheless those results are now the references for the next commits of Maespa.***

#### **Documentation modifications** 
--

#### **Bug fixed** 
The subroutine READLAT uses now the right KHRS time step indicated in *met.dat input file.

#### **Notes** 
--
<br/>
<br/>
<a name="0.1.6"></a>
<br/>
<br/>

# Version [0.1.6](#top) - Commit n° c78e6419/5211caf5 - Modifications of plantwater allocation and maestcom parameters

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
22-01-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 


- Modification of the allocation of plantwater in maindeclarations.f90
- Modification of the maestcom.f90 parameters to facilitate the test simulation process 


#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 

- In maindeclarations.f90 :  REAL PLANTWATER(MAXDATE+1, MAXT) was changed in REAL PLANTWATER(MAXDATE, MAXT)
- In maestcom.f90 : MAXT is set to 364 and MAXDATE to 177

#### **Code testing** 

The test case of Vezy et al 2018 has run for Plot_1 on 10 days to compare 2 simulations : this commit and the previous commit 6a55a565. The visualisation of the *watbal.dat output variables shows no differences.

#### **Documentation modifications** 
--

#### **Bug fixed** 
--
<br/>
<br/>
<a name="0.2.0"></a>
<br/>
<br/>

# Version [0.2.0](#top) - Commit n° 3667e79b - "Add dew activation option" + "Correctif watbal.f90"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
27-01-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Add of an option to activate dew from soil evaporation

#### **New features**  

Possibility to unactivate dew  from soil evaporation

#### **Usage of new features** 
In *_watpars.dat, the databloc ‘watcontrol’ must end with a line indicating the value of the new option ‘isimsoildew’’. If « isimsoildew » is set to 0, dew from soil evaporation is unactivated, else if isimsoildew=1,  the dew from soil evaporation is activated.

#### **Source code modifications** 

The new parameter «isimsoildew» is read by subroutines inputwatbal  and readwacontrol (inout.f90), which are called in the main program maespa.f90. Then, isimsoildew is transfered from the main program (maaespa.f90) to procedures findsoiltk, energyfun, energycalc and qeflux, where the condition IF((ISIMSOILDEW.EQ.0).AND.(QEFLUX.LT.0))QEFLUX=0. Is implemented. The files  maespa.f90, watbla.f90, maindeclarations.f90 and inout.f90 have then been modified.

#### **Code testing** 

The test case of Vezy et al 2018 has run for Plot_1 on 5 years to make 2 comparisons : 
- this commit with dew activated compared to the commit 32cc7e86 +diplacement of READLAT (equivalent of the last commit 6a55a565) (1)
- this commit with dew unactivated compared to the same commit with dew activated (2)
The differences between tests with and without dew are not visible for short simulations of 10 days.
Test (1) shows similar output files : the diff command between files does not give any results for a previous test performed on 10 days, and on 5 years, figures from 1_watbal.dat output files show no differences.
Test (2) suggest slight differences in the values of the output variables from the 1_watbal.dat file, as we expected when switching off dew. Those differences are visible in qc, qe, qh, qn, in the soilwat budget ‘wsoildiff-tfall+et+discharge’ and in the simulated soil evaporation.

#### **Documentation modifications** 
Not yet, but will be done when the documentation will be initiated in this repository

#### **Bug fixed** 
--

#### **Notes** 
We provide the test case ‘Plot_1_reftest’ in the inputfiles directory, with the default ‘isimsoildew’ variable which is turn to zero. This becomes the new reference test

<br/>
<br/>
<a name="0.2.1"></a>
<br/>
<br/>

# Version [0.2.1](#top) - Commit n° 28f34933 - "Unactivation of a condition in ZBRENT"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
01-02-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Unactivate a section in the ZBRENT function

#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 

In the function ZBRENT locaed in utils.f90 the condtion 'IF((FA.GT.0..AND.FB.GT.0.).OR.(FA.LT.0..AND.FB.LT.0.))THEN' has been commented.

#### **Code testing** 
The simulation at the Plot 1 corresponding to Vezy et al., 2018 have run for a 5 years simulation to compare :
- the results of the execution of the source code of the commit 28ec843f5
- the results of the execution of the source code of the commit 28ec843f5, where the section of ZBRENT has been unactivated
The results are similar for both simulations. The 'diff' command displays no differences when comparing output files (no rounding differences have been observed)

#### **Documentation modifications** 
--

#### **Bug fixed** 
--

#### **Notes** 
--

<br/>
<br/>
<a name="0.2.2"></a>
<br/>
<br/>

# Version [0.2.2](#top) - Commit n° e728521c - "Add of a CI script and modification of XYCOORDS allocation"

#### **Commit author**
C. Blitz Frayret and C. Diop

#### **Commit date** 
13-04-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the virtual machine hosting the Gitlab instance.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Add a 'gitlab-ci.yml' script for testing a 10 days simulation with specific tests programs 'compare_results.f90' and 'visu_watbal.Rmd'
Add specific files for a web site generation with Pages : Gemfile, index.html, public directory.
Inside the source code, the allocation of XYCOORDS array have been switch from static to dynamic.

#### **New features**  
A manual test has been developed to check the integrity of results from a given version with a reference simulation. 


#### **Usage of new features** 
This test is implemented in the .gitlab-ci.yml file. It compiles the /src files of Maespa with GNU compiler and launch a 10 days simulation for 1 plot. The results are copied in the directory inputfiles/Plot_1_reftest/results_to_test. 
This simulation is compared to a reference 10 days simulation located in the directory inputfile/Plot_1_reftest/results_10days_commit*. A sum of control is computed with the program 'compare_results.f90' for some output variables and the printed results (in the job report) allow to check if the difference between the reference simulation and the tested simulation is not to high.
After this test, a visualization script in RMarkdown tools/postprocessing/visu_watbal.Rmd plots some variables of the output file '1_watbal.dat' for the reference simulation and the tested simulation. The output results of this test are a pdf report calles 'visu_watbal.pdf' which can be download in the artifacts of the last job of visualization in the pipeline.
This test is to be triggered manually, in the section 'CI/CD', choose the option 'pipeline' and push r-the green buton 'run pipeline'.

#### **Source code modifications** 

In the module maestcom.f90, the XYCOORDS array has been dynamically allocated `real, dimension(:), allocatable :: xycoords` and the length NOALLTREES of the array  is set in the inout.f90 file (subroutine readxyz), once NOALLTREES is read from the output file. The instruction 'allocate(xycoords(noalltreesx2))' has been added in the subroutine readxyz.

#### **Code testing** 

The continuous integration script has been used to check if the modification of the allocation type of XYCOORDS changes the results. The output report 'visu_watbal.pdf' displays no differences between the reference and the tested simulation, except for the energy budget where such small values probably correspond to numerical inacuracies linked to the different machine where the reference simulation run. 

#### **Documentation modifications** 
--

#### **Bug fixed** 
--

#### **Notes** 
The reference simulation results must be changed manually in inputfile/Plot_1_reftest/results_10days_commit*, if the scientific results have to be modified by the new implementations. 
 

<br/>
<br/>
<a name="0.2.3"></a>
<br/>
<br/>

# Version [0.2.3](#top) - Commit n°9a355678 - "Modification of ITARGETS and ISPECIES allocations"

#### **Commit author**
C. Blitz Frayret and C. Diop

#### **Commit date** 
20-04-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the virtual machine hosting the Gitlab instance and on the Dell computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Allocations of ITARGETS and ISPECIES are originally static with the size of MAXT. Cheikh Diop modified the static allocations in dynamic allocations based on the length NOALLTREES, which is a parameter read in the 0001_trees.dat inout file.
The file .gitignore has been modified to include the Makefile and output data files. 

#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 

In the module maestcom.f90, the ITARGETS and ISPECIES arrays have been dynamically allocated `integer, dimension(:), allocatable :: itargets` , `integer, dimension(:), allocatable :: ispecies` and the length NOALLTREES is read from the output file 0001_trees.dat. The instructions 'allocate' have been added in the subroutine INPUTTREE (inout.f90).
Arguments of the followibng subroutines have also been modified by the deletion of itargets and ispecies : inputtree, sorttrees*, outputhr, outputdy, readspeclist and scaleup. These modifications are included in files inout.f90, watbal.f90, maespa.f90, maindeclarations.f90 (deletion of static allocations) and maestcom.f90 (add of 'allocatable').

#### **Code testing** 

The continuous integration script has been used to check if the modification of the allocation type ofISPECIES and ITARGETS changes the results. The output report 'visu_watbal.pdf' displays no differences between the reference and the tested simulation.
The simulation test of 10 days have also been performed manually on the Dell local computer, showing no differences with the master source code.

#### **Documentation modifications** 
--

#### **Bug fixed** 
--

#### **Notes** 
--
   
  
<br/>
<br/>
<a name="0.2.4"></a>
<br/>
<br/>

# Version [0.2.4](#top) - Commit n°5b1a45f0 01986ec8 1c1e906d- "Modification of TOTCO2, TOTRESPF ansd TOTRESPWM allocations"

#### **Commit author**
C. Blitz Frayret and C. Diop

#### **Commit date** 
25-04-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the virtual machine hosting the Gitlab instance.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

Allocations of TOTCO2, TOTRESPF and TOTRESPWM are originally static with the size of MAXT. Cheikh Diop modified the static allocations in dynamic allocations based on the length NOALLTREES, which is a parameter read in the 0001_trees.dat inout file.

#### **New features**  
--

#### **Usage of new features** 
--

#### **Source code modifications** 

In the module maestcom.f90, the arrays have been dynamically declared. The instructions 'allocate' have been added in the maain program maespa.f90.
Arguments of the followibng subroutines have also been modified since the arrays are now declared through the use of maestcom.f90 in each subroutine : outputdy, sumdaily and zerod. These modifications are included in files inout.f90, maespa.f90, maindeclarations.f90 (deletion of static allocations) and maestcom.f90 (add of 'allocatable').

#### **Code testing** 

The continuous integration script has been used to check if the modification of the allocation type ofISPECIES and ITARGETS changes the results. The output report 'visu_watbal.pdf' displays no differences between the reference and the tested simulation.

#### **Documentation modifications** 
--


<br/>
<br/>
<a name="0.2.5"></a>
<br/>
<br/>


# Version [0.2.5](#top) - Commit n° c3c7fdd3 - "Add of a range of time where *hrflux.dat file is generated"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
27-04-2021

#### **Compiler and configuration used**  
Commit validated with tests performed with a GNU based compilation, on the local Dell Precision T7610 computer.

#### **System requirements**  
 GNU compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 
These modifications generate two new parameters (ISTARTHR and IENDHR) to fix a range of time where *hrflux.dat file will be generated.
An additional FOLT variable has been added to the output file '*Dayflx.dat'.
The reference test input file 0001_met.dat has been corrected. 

#### **New features**  
Possibility to reduce the generation of *hrflux.dat file to a specific range of time included in the simulation period.

#### **Usage of new features** 
In the *confile.dat file, add of a databloc 'hrfluxdates', as followed : 

```
&hrfluxdates
startdatehr = '06/01/10'
enddatehr = '08/01/10'
/
```
 In this example, if the total period of the simulations ranges from 01/01/10 to 10/01/10, the days of year (DOY) where the file *hrflux.dat will be generated are : 2, 3, 4 and 5. Note that the first day of year has the number 0.
 This option is activated when the flag iohrly is set to 1. When set to 0, those dates are ignored since no *hrflux.dat file is generated.

We also added a column corresponding to the foliar surface of a tree (FOLT) in the output file '*_Dayflx.dat'.


#### **Source code modifications** 

Three source files and one input file are modified for this implementation : maespa.f90, inout.f90, maindeclarations.f90 and *_confile.dat.
In the input file *confile.dat, a new databloc with dates inclued in the simulated period `hrfluxdates` is set. 
In the inout.f90 file, new parameters 'ISTARTHR' and 'IENDHR' are set to read the *confile.dat new dates with the subroutine 'READDATES'. Those two variables are then transferred as arguments in the subroutines 'INPUTCON', and 'OUTPUTHR' in the file maespa.f90. In the subroutine 'OUTPUTHR', they are used in a condition on (IDAY+ISTART) to to print *hrflux.dat only if (ISTART+IDAY) is included between ISTARTHR and IENDHR.

#### **Code testing** 
Thés code have been tested with the continuous integration script. No differences is shown in the plot of the 1_watbal.dat file, however plots of the ZEN, absPAR and PAR variables from the 1_hrflux.dat file show differences because of the modification of the input parameters tzlong of the reference test. Those modifications are explained in the following section so this version will become the reference simulation in a next commit.

#### **Bug fixed** 
The descriptions of the variables 'PAR' and 'FBEAM' have been added in the header of the file '*_hrflux.dat'. 
The input file 0001_met.dat of the reference test Plot1_reftest has been corrected for two problems. First, between day 166 and 167, it has RAD values identically set to about 500 (probably a wrong type on Excel).This leads to a drop of the PAR, NIR and thermal radiations at these days. These RAD values have been changed according to a correct input file provided by Guerric Le Maire. 
Secondly, the parameter tzlong has been set to 45° instead of 150°. Indeed, this value of 45° was probably set to make PAR and ZEN curves in phase while the READLAT subroutine was not at the good place. But now that the subroutine READLAT has been moved after the read of the corresponding namelist (in getmet.f90, commit 6a55a565), the tzlong value of 150° lead to an offset between the PAR and the ZEN curves. We then set the tzlong value to 45°, corresponding to the meridian of the time zone of Brazil (location of this reference simulation).This modification allows the ZEN, PAR and absPAR curves to be in phase.

#### **Notes** 
This version will become the reference simulation because of the modification of TZLONG input parameter to 45° that allows to the variables PAR and absPAR to be in phase with ZEN).

#### **Bug fixed** 
--

#### **Notes** 
--
   
<br/>
<br/>
<a name="0.2.6"></a>
<br/>
<br/>

# Version [0.2.6](#top) - Commit n° 7d9540347 - "Modification header Dayflx/hrflux and revert previous allocation modifications"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
30-06-2021

#### **Compiler and configuration used**  
Commits validated with tests performed with an Intel based compilation, on the local Dell Precision T7610 computer and on the CI pipeline.

#### **System requirements**  
 GNU or Intel compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

- All the dynamic vectors allocations on the heap have been removed because this kind of allocation implies an explicit interface (such as maestcom.f90) and the loss of the subroutine arguments. Those arrays are : XYCOORDS, ITARGETS,ITARGETSI,ISPECIES,ISPECIESI,THDOWNP,WEIGHTS,WEIGHTSI, TOTH2OCAN,TOTRESPWM,EXPFACTORS,TOTRESPB,TOTRESPFR,TOTRESPCR,DOWNTHTREE,TOTHFX,TOTH2OEV,TOTRESPW,TARGETFOLS,TOTCO2,TOTH2O,TOTRESPF.
Since several subroutines are called further times with different actual arguments, the loss of arguments in dynamic allocations prevents from specifying which version of the actual arguments are used.
Static allocations on the stack have been tested because they can afford a late allocation once the array length is known (such as NOALLTREES insted of MAXT) but numerous variables are both declared as local in subroutines and global in the source code. The global allocations must be know during compiling so this allocation on the stack is neither appropriated.
 
- copies of variables used in the POINTSNEW, INTERPOLATEP and INTERPOLATET subroutines have been removed in the hourly loop and the corresponding arguments have been replaced by the variables in *SPEC.
- copies of variables in the hourly loop have been tagged and the unused ones have been commented.
- the headers of output files *Dayflux.dat and *hrflux.dat have been modified by Cheikh Diop to write the real name of the output variables (those found in the source code).


#### **New features**  
 -- 
 
#### **Usage of new features** 
--

#### **Source code modifications** 
See previous versions for the files modified in the deletion of dynamic allocations. For copies of variables and modification of output files headers, files inout.f90 and maespa.f90 have been modified.

#### **Code testing** 
Test of a 10 days simulation with an Intel based compilation on the Dell Precision computer displays an agreement between results in the reference simulation and the results of this commit. Same result with the CI.

#### **Documentation modifications** 
--

#### **Bug fixed** 
--

#### **Notes** 
--

<br/>
<br/>
<a name="0.2.7"></a>
<br/>
<br/>

# Version [0.2.7](#top) - Commit n°b62673e0a91a5b1d93638c85de942bcc53d12c98 - "Interpolatep subroutine displacement"

#### **Commit author**
C. Blitz Frayret

#### **Commit date** 
05-01-2022

#### **Compiler and configuration used**  
Commits validated with tests performed with an Intel based compilation, on the local Dell Precision T7610 computer and on the CI pipeline.

#### **System requirements**  
 GNU or Intel compiler, no specific librairies. For a Window based usage, a Linux emulator (MinGW) or a Integrated Development Environment (Visual Studio, CodeBlocks) is requiered. 

#### **Summary of these modifications** 

- After the rename of a group of variables corresponding to arguments of subroutines Interpolatep and Interpolatet (see last update and p. 19 of report 'Maespa_optimisation_memoire_subroutines.docx'), the subroutine Interpolatep frequently called in the hourly loop of maespa.f90 is moved outside this loop, in the daily loop (just before strating the hourly loop). This modification of the location of the subroutine Interpolatep allows a decrease in the number of calls with the same results and a gain of execution time of 10% for long simulations (see p.24 of 'Maespa_optimisation_memoire_subroutines.docx').


#### **New features**  
 -- 
 
#### **Usage of new features** 
--

#### **Source code modifications** 
See previous versions for the files modified in the deletion of dynamic allocations. For copies of variables and modification of output files headers, files inout.f90 and maespa.f90 have been modified.

#### **Code testing** 
Test of a 10 days simulation with an Intel based compilation on the Dell Precision computer displays an agreement between results in the reference simulation and the results of this commit. Same result with the CI. Additional test on the Dell precision for a 5 years simulation displaying the same results than the 5 years reference simulation. Although no execution time is earned for a 10 days simulation, the 5 years simulation displays a decrease of execution time of 10% with GNU compiler and 3% with Intel compiler (see 'Maespa_optimisation_memoire_subroutines.docx' and the following table).

| **Code modifications** | **GNU compiler** | **Intel compiler** |
| ------ | ------ |-----| 
| Code and makefile from previous Bitbucket repository | 525425 s REF TIME | > 1 week (86 days simulated in 3.5 days of execution) | 
|Makefile options + maestcom MAXT/MAXDATE | 292168 s (-45%) | 149185 s (-72%) |
|Makefile options + maestcom MAXT/MAXDATE + inlining technique in source code | 259694 s (-51%) | 125000 s (-76%) |
|Makefile options + maestcom MAXT/MAXDATE + inlining technique in source code + disp. Interpolatep| 206360 s (-61%) | 111315 s (-79%) |

#### **Documentation modifications** 
--

#### **Bug fixed** 
--

#### **Notes** 
--

