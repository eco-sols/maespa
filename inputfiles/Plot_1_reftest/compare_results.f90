program compare

	
	integer a, day, hour, rday, rhour, i, j
	real wsoil_B4, rwsoil_B4, soilevap_B4, rsoilevap_B4, &
	     discharge_B4, rdischarge_B4, et_B4, ret_B4, tfall_B4, rtfall_B4, ppt_B4, rppt_B4,&
	     canopystore_B4, rcanopystore_B4, evapstore_B4, revapstore_B4, qn_B4, rqn_B4,&
	     qe_B4, rqe_B4, qh_B4, rqh_B4, qc_B4, rqc_B4, tair_B4, rtair_B4, tcan_B4, rtcan_B4, &
	     taircan_B4, rtaircan_B4, test
    real percentage_wsoil, percentage_soilevap, percentage_discharge, percentage_et,&
          percentage_tfall, percentage_ppt, percentage_canopystore, percentage_evapstore,&
          percentage_qn, percentage_qe, percentage_qh, percentage_qc, &
          percentage_tair, percentage_tcan, percentage_taircan
    
     
	real  wsoil,wsoilroot,ppt,canopystore,evapstore,drainstore,tfall, &
	        et, etmeas, discharge, overflow, weightedswp, ktot, drythick, &
	        soilevap, soilmoist, fsoil, qh, qe, qn, qc, rglobund, rglobabv, &
	        radinterc, rnet, totlai, tair, taircan, tcan, tsoilsurf, tsoildry, & 
	        soilt1, soilt2, fracw1, fracw2, fracaPAR, VPDair, VPDaircan, GCAN, &
	        itertair, htot, rtherinc, sclosttot3, sclosttot, ev, drycan
	        
	 real   rwsoil,rwsoilroot,rppt,rcanopystore,revapstore,rdrainstore, &
		rtfall, ret, retmeas, rdischarge, roverflow, rweightedswp, rktot, rdrythick, &
	        rsoilevap, rsoilmoist, rfsoil, rqh, rqe, rqn, rqc, rrglobund, rrglobabv, &
	        rradinterc, rrnet, rtotlai, rtair, rtaircan, rtcan, rtsoilsurf, rtsoildry, & 
	        rsoilt1, rsoilt2, rfracw1, rfracw2, rfracaPAR, rVPDair, rVPDaircan, rGCAN, &
	        ritertair, rhtot, rrtherinc, rsclosttot3, rsclosttot, rev, rdrycan       
	        

	open(1,file='results_10days_simu_ref/1_watbal.dat')

	open(2,file='results_to_test/1_watbal.dat')


	rwsoil_B4=0
    wsoil_B4=0
    rsoilevap_B4=0
    soilevap_B4=0
    rdischarge_B4 =0
    discharge_B4=0
    ret_B4=0
    et_B4=0
    rtfall_B4=0
    tfall_B4 =0
    rppt_B4=0
    ppt_B4=0
    rcanopystore_B4=0
    canopystore_B4=0
    revapstore_B4=0
    evapstore_B4=0
    rqn_B4=0
    qn_B4=0
	rqe_B4=0
	qe_B4=0
	rqh_B4=0
	qh_B4=0
	rqc_B4=0
	qc_B4=0
	rtair_B4=0
	tair_B4=0
	rtcan_B4=0
	tcan_B4=0
	rtaircan_B4=0
	taircan_B4=0
	
	sum_rwsoil_B4=0
    sum_wsoil_B4=0
    sum_rsoilevap_B4=0
    sum_soilevap_B4=0
    sum_rdischarge_B4 =0
    sum_discharge_B4=0
    sum_ret_B4=0
    sum_et_B4=0
    sum_rtfall_B4=0
    sum_tfall_B4 =0
    sum_rppt_B4=0
    sum_ppt_B4=0
    sum_rcanopystore_B4=0
    sum_canopystore_B4=0
    sum_revapstore_B4=0
    sum_evapstore_B4=0
    sum_rqn_B4=0
    sum_qn_B4=0
	sum_rqe_B4=0
	sum_qe_B4=0
	sum_rqh_B4=0
	sum_qh_B4=0
	sum_rqc_B4=0
	sum_qc_B4=0
	sum_rtair_B4=0
	sum_tair_B4=0
	sum_rtcan_B4=0
	sum_tcan_B4=0
	sum_rtaircan_B4=0
	sum_taircan_B4=0

    do i=1,48
        read(1,*)
        read(2,*)
   enddo



	do 
		read(1,*,end=11) rday,rhour,rwsoil,rwsoilroot,rppt,rcanopystore,revapstore,rdrainstore, &
		rtfall, ret, retmeas, rdischarge, roverflow, rweightedswp, rktot, rdrythick, &
	        rsoilevap, rsoilmoist, rfsoil, rqh, rqe, rqn, rqc, rrglobund, rrglobabv, &
	        rradinterc, rrnet, rtotlai, rtair, rtaircan, rtcan, rtsoilsurf, rtsoildry, & 
	        rsoilt1, rsoilt2, rfracw1, rfracw2, rfracaPAR, rVPDair, rVPDaircan, rGCAN, &
	        ritertair, rhtot, rrtherinc, rsclosttot3, rsclosttot, rev, rdrycan       


		read(2,*) day,hour,wsoil,wsoilroot,ppt,canopystore,evapstore,drainstore, &
		tfall, et, etmeas, discharge, overflow, weightedswp, ktot, drythick, &
	        soilevap, soilmoist, fsoil, qh, qe, qn, qc, rglobund, rglobabv, &
	        radinterc, rnet, totlai, tair, taircan, tcan, tsoilsurf, tsoildry, & 
	        soilt1, soilt2, fracw1, fracw2, fracaPAR, VPDair, VPDaircan, GCAN, &
	        itertair, htot, rtherinc, sclosttot3, sclosttot, ev, drycan

		sum_wsoil=wsoil+wsoil_B4
		sum_rwsoil=rwsoil+rwsoil_B4

        sum_soilevap= soilevap+soilevap_B4
        sum_rsoilevap= rsoilevap+rsoilevap_B4
        
        sum_discharge= discharge+discharge_B4
        sum_rdischarge= rdischarge+rdischarge_B4
        
        sum_et= et+et_B4
        sum_ret= ret+ret_B4
        
        sum_tfall= tfall+tfall_B4
        sum_rtfall= rtfall+rtfall_B4
        
        sum_ppt= ppt+ppt_B4
        sum_rppt= rppt+rppt_B4
        
        sum_canopystore= canopystore+canopystore_B4
        sum_rcanopystore= rcanopystore+rcanopystore_B4
        
        sum_evapstore= evapstore+evapstore_B4
        sum_revapstore= revapstore+revapstore_B4
        
        sum_qn= qn+qn_B4
        sum_rqn= rqn+rqn_B4
                
        sum_qe= qe+qe_B4
        sum_rqe= rqe+rqe_B4
                
        sum_qh= qh+qh_B4
        sum_rqh= rqh+rqh_B4
                
        sum_qc= qc+qc_B4
        sum_rqc= rqc+rqc_B4
                
        sum_tair= tair+tair_B4
        sum_rtair= rtair+rtair_B4
                        
        sum_tcan= tcan+tcan_B4
        sum_rtcan= rtcan+rtcan_B4
                        
        sum_taircan= taircan+taircan_B4
        sum_rtaircan= rtaircan+rtaircan_B4

        
		wsoil_B4=wsoil
		rwsoil_B4=rwsoil
		
		soilevap_B4=soilevap
		rsoilevap_B4=rsoilevap
		        
        discharge_B4= discharge
        rdischarge_B4= rdischarge

        
        et_B4= et
        ret_B4= ret
        
        tfall_B4= tfall
        rtfall_B4= rtfall
        
        ppt_B4= ppt
        rppt_B4= rppt
        
        scanopystore_B4= canopystore
        rcanopystore_B4= rcanopystore
        
        evapstore_B4= evapstore
        revapstore_B4= revapstore
        
        qn_B4= qn
        rqn_B4= rqn
                
        qe_B4= qe
        rqe_B4= rqe
                
        qh_B4= qh
        rqh_B4= rqh
                
        qc_B4= qc
        rqc_B4= rqc
                
        tair_B4= tair
        rtair_B4= rtair
                        
        tcan_B4= tcan
        rtcan_B4= rtcan
                        
        taircan_B4= taircan
        rtaircan_B4= rtaircan

	enddo

11	continue

	!write(*,*) 'somme rwsoil=', sum_rwsoil
	!write(*,*) 'somme wsoil=', sum_wsoil
    	
	!close(1)
	!close(2)
	if(sum_rwsoil.eq.0)then
	percentage_wsoil=0
	else	
	percentage_wsoil= (100/sum_rwsoil)*abs(sum_rwsoil-sum_wsoil)
	endif
	write(*,*) 'percentage of difference wsoil =', percentage_wsoil
	
	!write(*,*) 'somme rsoilevap=', sum_rsoilevap
	!write(*,*) 'somme soilevap=', sum_soilevap
	
	if(sum_rsoilevap.eq.0)then
	percentage_soilevap=0
	else
	percentage_soilevap = (100/sum_rsoilevap)*abs(sum_rsoilevap-sum_soilevap)
	endif
	write(*,*) 'percentage of difference soilevap=', percentage_soilevap
	
	
	!write(*,*) 'somme rdischarge=', sum_rdischarge
	!write(*,*) 'somme discharge=', sum_discharge
	if(sum_rdischarge.eq.0)then
	percentage_discharge=0
	else
	percentage_discharge = (100/sum_rdischarge)*abs(sum_rdischarge-sum_discharge)
	endif
	
	write(*,*) 'percentage of difference discharge=', percentage_discharge
	
	!write(*,*) 'somme ret=', sum_ret
	!write(*,*) 'somme et=', sum_et
	if(sum_ret.eq.0)then
	percentage_et=0
	else
	percentage_et = (100/sum_ret)*abs(sum_ret-sum_et)
	endif
	write(*,*) 'percentage of difference et=', percentage_et
	
	!write(*,*) 'somme rtfall=', sum_rtfall
	!write(*,*) 'somme tfall=', sum_tfall
	if(sum_rtfall.eq.0)then
	percentage_tfall = 0
	else
	percentage_tfall = (100/sum_rtfall)*abs(sum_rtfall-sum_tfall)
	endif
	write(*,*) 'percentage of difference tfall=', percentage_tfall
	
	!write(*,*) 'somme rppt=', sum_rppt
	!write(*,*) 'somme ppt=', sum_ppt
	if(sum_rppt.eq.0)then
	percentage_ppt=0
	else
	percentage_ppt = (100/sum_rppt)*abs(sum_rppt-sum_ppt)
	endif
	write(*,*) 'percentage of difference ppt=', percentage_ppt
	
	
	!write(*,*) 'somme rcanopystore=', sum_rcanopystore
	!write(*,*) 'somme canopystore=', sum_canopystore
	if(sum_rcanopystore.eq.0)then
	percentage_canopystore=0
	else
	percentage_canopystore = (100/sum_rcanopystore)*abs(sum_rcanopystore-sum_canopystore)
	endif
	write(*,*) 'percentage of difference canopystore=', percentage_canopystore
	
	!write(*,*) 'somme revapstore=', sum_revapstore
	!write(*,*) 'somme evapstore=', sum_evapstore
	if(sum_revapstore.eq.0)then
	percentage_evapstore = 0
	else
	percentage_evapstore = (100/sum_revapstore)*abs(sum_revapstore-sum_evapstore)
	endif
	write(*,*) 'percentage of difference evapstore=', percentage_evapstore
	
	!write(*,*) 'somme rqn=', sum_rqn
	!write(*,*) 'somme qn=', sum_qn
	if(sum_rqn.eq.0)then
	percentage_qn = 0
	else
	percentage_qn = (100/sum_rqn)*abs(sum_rqn-sum_qn)
	endif
	write(*,*) 'percentage of difference qn=', percentage_qn
	
	!write(*,*) 'somme rqe=', sum_rqe
	!write(*,*) 'somme qe=', sum_qe
	if(sum_rqe.eq.0)then
	percentage_qe=0
	else
	percentage_qe = (100/sum_rqe)*abs(sum_rqe-sum_qe)
	endif
	write(*,*) 'percentage of difference qe=', percentage_qe
	
	!write(*,*) 'somme rqh=', sum_rqh
	!write(*,*) 'somme qh=', sum_qh
	if(sum_rqh.eq.0)then
	percentage_qh = 0
	else
	percentage_qh = (100/sum_rqh)*abs(sum_rqh-sum_qh)
	endif
	write(*,*) 'percentage of difference qh=', percentage_qh
	
	!write(*,*) 'somme rqc=', sum_rqc
	!write(*,*) 'somme qc=', sum_qc
	if(sum_rqc.eq.0)then
	percentage_qc = 0
	else
	percentage_qc = (100/sum_rqc)*abs(sum_rqc-sum_qc)
	endif
	write(*,*) 'percentage of difference qc=', percentage_qc
	
	!write(*,*) 'somme rtair=', sum_rtair
	!write(*,*) 'somme tair=', sum_tair
	if(sum_rtair.eq.0)then
	percentage_tair = 0
	else
	percentage_tair = (100/sum_rtair)*abs(sum_rtair-sum_tair)
	endif
	write(*,*) 'percentage of difference tair=', percentage_tair
	
	!write(*,*) 'somme rtcan=', sum_rtcan
	!write(*,*) 'somme tcan=', sum_tcan
	if(sum_rtcan.eq.0)then
	percentage_tcan = 0
	else
	percentage_tcan = (100/sum_rtcan)*abs(sum_rtcan-sum_tcan)
	endif
	write(*,*) 'percentage of difference tcan=', percentage_tcan
	
	!write(*,*) 'somme rtaircan=', sum_rtaircan
	!write(*,*) 'somme taircan=', sum_taircan
	if(sum_rtaircan.eq.0)then
	percentage_taircan = 0
	else
	percentage_taircan = (100/sum_rtaircan)*abs(sum_rtaircan-sum_taircan)
	endif
	
	write(*,*) 'percentage of difference taircan=', percentage_taircan

        
	end program compare	
