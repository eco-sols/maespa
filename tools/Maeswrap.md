# Use of Maeswrap R package 

*(Autored by R. Vezy, G. Le Maire, R. Duursma)*


A companion R package is distributed on [CRAN](https://cran.r-project.org/web/packages/Maeswrap/index.html) and on [Github]([Maeswrap](https://github.com/RemkoDuursma/Maeswrap/tree/master/R)).

- Download the last CRAN version using:

```r
install.packages("Maeswrap")
```

- Or the latest development version using:

```r
devtools::install_github("RemkoDuursma/Maeswrap")
# Or if you don't have devtools:
remotes::install_github("RemkoDuursma/Maeswrap")
```
- Go in the working directory where input files are located

- Modifications of input files :

	* **replacePAR()** : change a parameter value

	* arguments :
	
		**datfile** = namme of input file  
		**namelist** = name of the data list (NAME)  
		**parname** = name of the parameter (VAR1, VAR2)  

	In the source files, the read of input files in Maespa source files uses the Fortran namelist format with the following notation : 

	NAMELIST /NAME/ VAR1, VAR2

	In the input files, the above namelist should be writen :

	&NAME  
	VAR1=0.05  
	VAR2=370.1  
	

	Arrays of numbers should be specified as space-separated numbers :  
	&NAME  
	VAR2= 0.05 370.1

	Example : 

	replacePAR(datfile="0.0002_trees.dat", parname="notrees", namelist="plot", 15)  
	replacePAR(datfile="0.0002_trees.dat", parname="Y0", namelist="plot", 0)  
	replacePAR(datfile="0.0002_trees.dat", parname="XMAX", namelist="plot", 12)  