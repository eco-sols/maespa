# Pre processing 

* [Maeswrap](tools/Maeswrap.md)




# Post processing 

* Maeswrap

    *Document under construction*


* Output files vizualisation

     Those scripts xreate *jpeg* plots of several variables. To use them, place them in the same directory of the output files, launch RStudio and push `source`button or type the following command in the terminal :
     ```bash
     source('visu_watbal.R')
     ```
    Without opening RStudio, these scripts can also be activated with a Unix terminal and the following command:
    ```bash
    Rscript visu_watbal.R
    ```

    - [R script to vizualise variables of output file 1_watbal.dat](postprocessing/visu_watbal.R)

    - [R script to vizualise variables of output file 1_Dayflux.dat]()
    
    *Document under construction*

